<p align="center"><img src="https://i.imgur.com/iLLX7A8.png"></p>
<h4 align="center">
Получить точное местоположение с использованием поддельного сайта
</h4>

<p align="center">
<img src="https://img.shields.io/badge/Python-3-brightgreen.svg?style=plastic">
<img src="https://img.shields.io/badge/Python-2-brightgreen.svg?style=plastic">
<img src="https://img.shields.io/badge/Docker-✔-blue.svg?style=plastic">
<img src="https://img.shields.io/badge/Termux-✔-red.svg?style=plastic">
<img src="https://img.shields.io/badge/NetHunter-✔-red.svg?style=plastic">
</p>

Концепция Seeker проста, точно так же, как мы размещаем фишинговые страницы для получения учетных данных, почему бы не разместить поддельную страницу, которая запрашивает ваше местоположение, как многие популярные веб-сайты, основанные на местоположении.

Искатель размещает поддельный веб-сайт на **в встроенном PHP-сервере** и использует **Ngrok**, веб-сайт запрашивает разрешение на местоположение, и, если пользователь это разрешает, мы можем получить:

* Долгота
* Широта
* Точность
* Высота - не всегда доступна
* Направление - доступно только если пользователь движется
* Скорость - Доступно только если пользователь движется

Наряду с информацией о местоположении мы также получаем **информацию об устройстве** без каких-либо разрешений:

* Операционная система
* Платформа
* Количество ядер процессора
* Объем оперативной памяти - приблизительные результаты
* Разрешение экрана
* Информация о GPU
* Имя и версия браузера
* Публичный IP-адрес

**Этот инструмент является проверкой концепции и предназначен только для образовательных целей. Seeker показывает, какие данные может собирать вредоносный веб-сайт о вас и ваших устройствах, и почему вы не должны нажимать на случайные ссылки и разрешать критические разрешения, такие как местоположение и т. Д.**

*Другие инструменты и сервисы предлагают IP Geolocation, который не очень точен и не определяет местоположение пользователя.

*Как правило, если пользователь принимает разрешение на определение местоположения, точность полученной информации составляет **с точностью приблизительно до 30 метров**.

**Примечание **: На iPhone по какой-то причине точность определения местоположения составляет приблизительно 65 метров.

## Проверено на:

* Kali Linux 2018.2
* Ubuntu 18.04
* Arch Linux based Distro
* Termux
* Kali Linux (WSL)
* Parrot OS
* Zorin OS

## Установка

### Ubuntu / Kali Linux

```bash
git clone https://github.com/thewhiteh4t/seeker.git
cd seeker/
chmod 777 install.sh
./install.sh

# OR using Docker

# Install docker

curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

# Build Seeker
cd seeker/
docker build -t seeker .

# Launch seeker
docker run -t --rm seeker
```

[![asciicast](https://asciinema.org/a/195052.png)](https://asciinema.org/a/195052)

### Arch Linux Based Distro

```bash
# Install docker

pacman -Syy
pacman -S docker
systemctl start docker.service

# Build Seeker
cd seeker/
docker build -t seeker .

# Launch seeker
docker run -t --rm seeker
```
### Termux

```bash
cd seeker/termux
chmod 777 install.sh
./install.sh
```

>Если вы не можете получить URL-адрес ngrok, что означает, что ngrok не может разрешить DNS, переключитесь на мобильные данные вместо WiFi, и это должно работать, это проблема с ngrok.

[![asciicast](https://asciinema.org/a/195830.png)](https://asciinema.org/a/195830)

## Demo

Youtube - https://www.youtube.com/watch?v=ggUGPq4cjSM
