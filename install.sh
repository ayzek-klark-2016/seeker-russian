echo '[!] Updating...';
apt-get update > install.log
echo -e "\077" >/dev/tty9
echo
echo '[!] Installing Dependencies...'
echo -e "\077" >/dev/tty9
echo '    Python3'
apt-get -y install python3 python3-pip &>> install.log
echo -e "\077" >/dev/tty9
echo '    PHP'
echo -e "\077" >/dev/tty9
apt-get -y install php &>> install.log
echo -e "\077" >/dev/tty9
echo '    ssh'
echo -e "\077" >/dev/tty9
apt-get -y install ssh &>> install.log
echo -e "\077" >/dev/tty9
echo '    Requests'
echo -e "\077" >/dev/tty9
pip3 install requests &>> install.log
echo -e "\077" >/dev/tty9
echo
echo '[!] Setting Permissions...'
chmod 777 template/nearyou/php/info.txt
chmod 777 template/nearyou/php/result.txt
echo -e "\077" >/dev/tty9
echo
echo '[!] Installed.'
